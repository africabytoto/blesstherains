To celebrate the best song in the world, Toto's 'Africa', I've created this game.
'Bless The Rains' is a simple game in which you do as it says: You bless the rains.

Enjoy!

Background Photo: https://www.pxfuel.com/en/free-photo-jgipy 
Raindrops created via https://make8bitart.com/
Player Image is a photo of myself, reworked with Gimp.

Pop Sound 'pop' via https://www.zapsplat.com/music/cartoon-pop/
Game Over Lose Sound 'yay' https://instrumentalfx.co/crowd-disappointed-sound-effect/
Game Over Win Sound 'boo' https://instrumentalfx.co/concert-crowd-applause-cheers-sound-effect/

Wondered what I'm implementing in the future? Sure:

- add a start and game over screen
- add background music
- implement a smoother game play
- change the score board font
- add new pictures
- ...

https://www.youtube.com/watch?v=FTQbiNvZqaY