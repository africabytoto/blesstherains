package de.awacademy;

        import javafx.scene.media.Media;
        import javafx.scene.media.MediaPlayer;

        import java.nio.file.Paths;

public class Sound {

    private String pop = "src/de/awacademy/sound/pop.mp3";
    private String yay = "src/de/awacademy/sound/yay.mp3";
    private String boo = "src/de/awacademy/sound/boo.mp3";

    MediaPlayer mediaPlayer;

    public void music(String bip) {
        Media hit = new Media(Paths.get(bip).toUri().toString());
        mediaPlayer = new MediaPlayer(hit);
        mediaPlayer.play();
    }



    public String getPop() {
        return pop;
    }

    public String getYay() {
        return yay;
    }

    public String getBoo() {
        return boo;
    }
}
