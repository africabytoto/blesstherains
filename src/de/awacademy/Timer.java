package de.awacademy;

import de.awacademy.model.Model;
import javafx.animation.AnimationTimer;

import java.io.FileNotFoundException;

public class Timer extends AnimationTimer {

    private Graphics graphics;
    private Model model;
    Sound sound1 = new Sound();

    public Timer(Graphics graphics, Model model) {
        this.graphics = graphics;
        this.model = model;
    }

    // gc.fillText("YOU'VE WON!", 17, 167);
    @Override
    public void handle(long nowNano) {
        model.update(nowNano);
        try {
            if (model.getPlayer().getScore() == 10) {
                sound1.music(sound1.getYay());
                System.out.println("YOU'VE WON!");
                stop();
            } else if (model.getPlayer().getScore() < 0) {
                sound1.music(sound1.getBoo());
                System.out.println("GAME OVER!");
                stop();
            }

            graphics.draw();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}
