package de.awacademy.model;

import java.util.Random;

public class Rains {
//Namen der Klassen sprechender zu machen - "Anfängerfehler"
    private int x;
    private double y;
    private int h;
    private int w;
    private int speed;
    private int randomImg = 0;

    private static int randomInt(int min, int max) {
        Random r = new Random();
        return r.nextInt((3 - 1) + 1) + 1;
    }

    public Rains(int x) {

        this.randomImg = randomInt(1, 3);
        this.x = x;
        y = 0;
        h = 50;
        w = 50;
    }

    public int getRandomImg() {
        return randomImg;
    }

    public void drop(double i) {
        y += i-0.5;
    }

    public int getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public int getH() {
        return h;
    }

    public int getW() {
        return w;
    }


}
