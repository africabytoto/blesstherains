package de.awacademy.model;

import de.awacademy.Sound;

import java.util.*;
import java.util.List;

public class Model {
    int counter = 20;
    int acidCounter = 20;
    private Player player;
    private List<Rains> rainList = new LinkedList<Rains>();
    private List<AcidRains> acidRainsList = new LinkedList<AcidRains>();
    boolean gameOver = false;
    boolean gameOverWin = false;

    private static int randomInt(int min, int max) {
        Random r = new Random();
        return r.nextInt((500 - 0) + 1) + 0;
    }

    public final double WIDTH = 600;
    public final double HEIGHT = 800;

    public Model() {
        this.player = new Player(0, -10);
    }

    Sound sound1 = new Sound();


    public Player getPlayer() {
        return player;
    }

    public List<Rains> getRains() {
        return rainList;
    }

    public List<AcidRains> getAcidRains() {
        return acidRainsList;
    }


    public void makeItRain() {
        rainList.add(new Rains(randomInt(50, 420)));
    }

    public void makeItRainAcid() {
        acidRainsList.add(new AcidRains(randomInt(50, 420)));
    }

    public boolean isGameOver() {
        return gameOver;
    }

    public boolean isGameOverWin() {
        return gameOverWin;
    }


    public void update(long nowNano) {

        counter++;
        if (rainList.isEmpty() || counter % 20 == 0) {
            makeItRain();
        }

        Rains index = null;

        acidCounter++;
        if (acidRainsList.isEmpty() || acidCounter % 20 == 0) {
            makeItRainAcid();
        }

        AcidRains indexA = null;


        for (int l = 0; l <= 10; l++) {

            for (AcidRains acidRains : acidRainsList) {
                acidRains.drop(1);

                indexA = acidRains;
            }


        }

        if (indexA != null) {
            acidRainsList.remove(indexA);
        }

        for (int k = 0; k <= 10; k++) {


            for (Rains rains : rainList) {

                rains.drop(1);

                if (rains.getY() + rains.getH() > player.getY()
                        && rains.getX() >= player.getX()
                        && (player.getX() + player.getW()) >= rains.getX()) {

                    sound1.music(sound1.getPop());
                    player.incScore();
                    index = rains;

                    System.out.println(player.getScore());
                }

                if (rains.getY() + rains.getH() > HEIGHT) {
                    player.decScore();
                    index = rains;

                    if (player.getScore() >= 1)
                        System.out.println(player.getScore());

                    if (player.getScore() < 0) {
                        gameOver = true;

                    } else if (player.getScore() > 10) {
                        gameOver = true;
                    }
                }
            }
            if (index != null) {
                rainList.remove(index);
            }
        }
    }

    public int coPos(int posPlayer, int posRains) {
        return Math.abs(posPlayer - posRains);
    }

    public int coSize(int sizePlayer, int sizeRains) {
        return Math.abs(sizePlayer - sizeRains);
    }

}

