package de.awacademy.model;

import java.util.ArrayList;
import java.util.List;

public class Player {

    private int x;
    private int y;
    private int h;
    private int w;
    private int score;
    private List<Rains> rains;
    private List<AcidRains> acidRains;

    //x & y Startpunkt Kreis
    //h & w Größe Kreis

    public Player(int h, int w) {
        this.x = 230;
        this.y = 680;
        this.h = 100;
        this.w = 100;
        this.score = 0;

        initPlayer();
    }


    private void initPlayer() {

        rains = new ArrayList<>();
    }

    public void move(int dx) {
        if (x + dx >= 10 && x + dx <= 590 - w) {
            this.x += dx;
        }
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getH() {
        return h;
    }

    public int getW() {
        return w;
    }

    public int getScore() {
        return score;
    }

    public void incScore() {
        score += 1;
    }

    public void decScore() {
        score -= 1;
    }


}
