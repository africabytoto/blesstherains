package de.awacademy;

import de.awacademy.model.Model;
import javafx.scene.input.KeyCode;

public class InputHandler {

    private Model model;

    public InputHandler (Model model) {
        this.model = model;
    }

    public void onKeyPressed(KeyCode keycode) {
        // was passiert, wenn ich bestimmte Knöpfe drücke


        if(keycode == KeyCode.LEFT) {
            model.getPlayer().move(-30);
        }
        if(keycode == KeyCode.RIGHT) {
            model.getPlayer().move(30);
        }
    }
}
