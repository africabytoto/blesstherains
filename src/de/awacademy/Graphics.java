package de.awacademy;

import de.awacademy.model.AcidRains;
import de.awacademy.model.Model;
import de.awacademy.model.Rains;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;


public class Graphics {

    ArrayList<Image> imageList = new ArrayList<Image>();
    Image rainsImage1;
    Image rainsImage2;
    Image rainsImage3;
    Image bg;
    Image player;
    Image logo;
    Text text = new Text();
    Image acidRains;
    private GraphicsContext gc;
    private Model model;

    public Graphics(GraphicsContext gc, Model model) {
        this.gc = gc;
        this.model = model;

        try {
            rainsImage1 = new Image(new FileInputStream("src/de/awacademy/images/raindrop1.png"));
            rainsImage2 = new Image(new FileInputStream("src/de/awacademy/images/raindrop2.png"));
            rainsImage3 = new Image(new FileInputStream("src/de/awacademy/images/raindrop3.png"));
            bg = new Image(new FileInputStream("src/de/awacademy/images/bg.png"));
            player = new Image(new FileInputStream("src/de/awacademy/images/player.png"));
            logo = new Image(new FileInputStream("src/de/awacademy/images/africa.png"));
            acidRains = new Image(new FileInputStream("src/de/awacademy/images/acidraindrop.png"));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        imageList.add(rainsImage1);
        imageList.add(rainsImage2);
        imageList.add(rainsImage3);
    }

    public void draw() throws FileNotFoundException {

        gc.drawImage(bg, 0, 0, model.WIDTH, model.HEIGHT);
        gc.drawImage(player, model.getPlayer().getX(), model.getPlayer().getY(),
                model.getPlayer().getW(), model.getPlayer().getH());

        //gc.clearRect(0, 0, model.WIDTH, model.HEIGHT);
        if (!model.getRains().isEmpty()) {

            for (Rains rainy : model.getRains()) {
                gc.drawImage(imageList.get(rainy.getRandomImg() - 1), rainy.getX(), rainy.getY(),
                        rainy.getW(), rainy.getH());
            }
        }

        if (!model.getAcidRains().isEmpty()) {

            for (AcidRains acidRainy : model.getAcidRains()) {
                gc.drawImage(acidRains, acidRainy.getX(), acidRainy.getY(),
                        acidRainy.getW(), acidRainy.getH());
            }
        }






       /* } else if (model.getPlayer().getScore() >9) {
            gc.fillText("YOU'VE WON", 17, 180);

        }*/

        if (model.getPlayer().

                getScore() >= 0) {
            gc.fillText("SCORE: " + model.getPlayer().getScore(), 17, 167);

        } else if (model.getPlayer().

                getScore() < 0) {
            gc.fillText("GAME OVER", 17, 167);
        }
        gc.drawImage(logo, 0, 1);
        gc.setFont(Font.font("Arial", 20));
        gc.setFill(Color.WHITE);
    }


}